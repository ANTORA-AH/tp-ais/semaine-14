= AIS-SEMAINE-15-VSHERE-V7-08-HA
:author: Arnaud Harbonnier
:email: arnaud.harbonnier@valarep.fr
:title-page:
//firstname middlename lastname <email>firstname middlename lastname <email>; firstname middlename lastname <email>
//Arnaud Harbonnier <arnaud.harbonnier@gmail.com>
//Arnaud Harbonnier <arnaud.harbonnier@valarep.fr>
//Arnaud Harbonnier <arnaud.harbonnier@ltpdampierre.fr>
//doctype: book
:sectnums:
:toc:
:toc-title:
:toclevels: 4
:source-highlighter: highlightjs
//icons: font
:imagesdir: ./images/AIS-VMWARE-08-HA
:sectanchors: 
//image::sunset.jpg[Sunset,300,400]
//image::sunset.jpg[alt=Sunset,width=300,height=400]
:revnumber: 2022-2023
:revdate: VALAREP
:revremark: TP - AIS
:version-label!: 
:version-label: Version
:experimental:
:icons: font
//:icon-set: fa
////
You can use font-based icons from any of the following icon sets in your PDF document:
fa - Font Awesome 4 (default, deprecated)
fas - Font Awesome - Solid
fab - Font Awesome - Brands
far - Font Awesome - Regular
fi - Foundation Icons
pf - Payment font (deprecated)
////
// saut da page <<<


== INFRA

[plantuml, target=vmware-01, format=png]
----
nwdiag {
      
  network STORAGE-vSwitch2 {
      address = "192.168.1.0/24"

      STORAGE [address = "        .254"];
      ESX01 [address = "      .1"];
      ESX02 [address = "      .2"];
  }

  network MGMT-vSwitch0 {
      address = "172.16.20.0/24";

      ESX01 [address = "      .1"];
      ESX02 [address = "      .2"];
      DC01 [address = "      .10"];
      ESX03 [address = "      .3"];
  }

}
----

== vSphere HA

Fonction qui assure la haute disponibilité (failover) de nos VMs. +
En cas de perte d’un des ESXI, les VMs qui étaient exécutées par lui séteignent et redémarrent automatiquement sur les autres ESXi disponibles au sein du cluster. On a donc une coupure de service de courte durée.

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha00.png[] 

== Pré-requis

* Tous les hôtes sont configurés en statique
* Réseau de gestion (MGMT) - le même que le vCenter
* Tous les hôtes du cluster doivent avoir accès au même stockage partagé et mêmes réseaux

== Configuration du HA

=== Pannes et réponses

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha01.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha02.png[]

[NOTE]
La surveillance de l'hôte est activée par défaut

=== Contrôle d'admission

[NOTE]
Mécanisme qui permet de réserver des ressources CPU et mémoire au niveau du cluster en cas de panne. Nous le désactiverons pour les tests sinon nous aurons du mal à déclencher les bascules. En général on laisse le choix par défaut _Pourcentage de ressources du cluster_.

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha03.png[] 

=== Banques de données de signal de pulsation

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha04.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha05.png[] 

== TEST

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha06.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha07.png[] 

Simulons une panne en mettant en pause l'ESX-002

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha08.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha11.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha12.png[] 

[WARNING]
ça ne fonctionne pas

== TROUBLESHOOTING : Problème d'adresse d'isolation

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha14.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha15.png[] 

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha16.png[] 

[NOTE]
Une adresse appelée *_adresse d’isolation_* est nécessaire pour chaque hôte dans le réseau de Gestion. Dans le cas où celui-ci ne reçoit plus de signal heartbeat, il se retrouve alors isolé. Avant de prendre une décision et pour qu’il soit sûr qu’il n’y ait pas de problème réseau, l’hôte tente de faire un PING vers cette adresse d’isolation *_das.isolationaddress0_*. +
Dans *_Options avancées_*, il est donc possible de choisir une adresse personnalisée. Mettre, par exemple, l'adresse du vCenter en tant qu’adresse d’isolation.

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha17.png[]

[WARNING]
désormais ça fonctionne !

image:ROOT:AIS-VMWARE-V7-08-HA/conf-ha18.png[]

[NOTE]
Quand on remet l'hôte en fonctionnement la VM reste sur l'hôte sur lequel elle a été déplacée.

[NOTE]
Principe du HA : La VM est éteinte et redémarrée sur un autre hôte du cluster
