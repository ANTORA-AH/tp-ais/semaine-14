= AIS-SEMAINE-14-VSHERE-V7-02-INSTALL-VCSA
:author: Arnaud Harbonnier
:email: arnaud.harbonnier@valarep.fr
:title-page:
//firstname middlename lastname <email>firstname middlename lastname <email>; firstname middlename lastname <email>
//Arnaud Harbonnier <arnaud.harbonnier@gmail.com>
//Arnaud Harbonnier <arnaud.harbonnier@valarep.fr>
//Arnaud Harbonnier <arnaud.harbonnier@ltpdampierre.fr>
//doctype: book
:sectnums:
:toc:
:toc-title:
:toclevels: 4
:source-highlighter: highlightjs
//icons: font
:imagesdir: ./images/AIS-SEMAINE-14-VMWARE-02-INSTALL-VCSA
:sectanchors: 
//image::sunset.jpg[Sunset,300,400]
//image::sunset.jpg[alt=Sunset,width=300,height=400]
:revnumber: 2022-2023
:revdate: VALAREP
:revremark: TP - AIS
:version-label!: 
:version-label: Version
:experimental:
:icons: font
//:icon-set: fa
////
You can use font-based icons from any of the following icon sets in your PDF document:
fa - Font Awesome 4 (default, deprecated)
fas - Font Awesome - Solid
fab - Font Awesome - Brands
far - Font Awesome - Regular
fi - Foundation Icons
pf - Payment font (deprecated)
////
// saut da page <<<


== INFRA

[plantuml, target=vmware-01, format=png]
----
nwdiag {
      
  network STORAGE-vSwitch2 {
      address = "192.168.1.0/24"

      STORAGE [address = "        .254"];
      ESX01 [address = "      .1"];
      ESX02 [address = "      .2"];
  }

  network MGMT-vSwitch0 {
      address = "172.16.20.0/24";

      ESX01 [address = "      .1"];
      ESX02 [address = "      .2"];
      DC01 [address = "      .10"];
      ESX03 [address = "      .3"];
  }

}
----

== Bible vmware

https://docs.vmware.com[Documentation officielle vmware]

== Installation du VCSA (depuis DC01)

*Le vCenter Server Appliance*

vCenter Server Appliance est une machine virtuelle :

* Linux VMware Photon,
* Prépackagée pour vCenter Server,
* Base de données PostgreSQL,
* Maximums : 2000 ESXi, 35000 Vms enregistrées, 25000 Vms en fonctionnement,
* Fonctionnalités supplémentaires : vCenter Server HA, Sauvegarde et restauration.

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa01.png[]

=== Etape 1

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa02.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa03.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa04.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa05.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa06.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa07.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa08.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa09.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa10.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa11.png[]

[NOTE]
25 minutes environ

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa12.png[]


*Ajouter les enregistrements A et PTR de vcsa dans le DNS local*

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa-dns-direct.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa-dns-inverse.png[] 

=== Etape 2

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa13.png[] 

[WARNING]
A la copie d'écran suivante : mettre *Administrator* et nom de domaine *vsphere.local*

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa14.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa15.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa16.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa17.png[] 

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa18.png[]

[NOTE]
25 minutes environ

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa19.png[]

== Test de connexion au VCSA sur le port 443

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa20.png[]

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa21.png[]

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa22.png[]


== Test de connexion au VCSA sur le port 5480

Aministration du VCSA

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa23.png[]

image:ROOT:AIS-VMWARE-V7-02-INSTALL-VCSA/vcsa24.png[]

[TIP]
https://angrysysops.com/2022/07/22/how-to-reset-the-administratorvsphere-local-password/[réinitialiser mot de passe administrator VCSA - Lien 1] +
https://angrysysops.com/2021/12/17/error-when-uploading-files-to-vcenter-server-appliance-using-winscp/[réinitialiser mot de passe administrator VCSA - Lien 2]