= AIS-SEMAINE-15-VSHERE-V8-01-INFRA
:author: Arnaud Harbonnier
:email: arnaud.harbonnier@valarep.fr
:title-page:
//firstname middlename lastname <email>firstname middlename lastname <email>; firstname middlename lastname <email>
//Arnaud Harbonnier <arnaud.harbonnier@gmail.com>
//Arnaud Harbonnier <arnaud.harbonnier@valarep.fr>
//Arnaud Harbonnier <arnaud.harbonnier@ltpdampierre.fr>
//doctype: book
:sectnums:
:toc:
:toc-title:
:toclevels: 4
:source-highlighter: highlightjs
//icons: font
:imagesdir: ./images/AIS-SEMAINE-15-VMWARE-VSPHERE-V8-01-INFRA
:sectanchors: 
//image::sunset.jpg[Sunset,300,400]
//image::sunset.jpg[alt=Sunset,width=300,height=400]
:revnumber: 2022-2023
:revdate: VALAREP
:revremark: TP - AIS
:version-label!: 
:version-label: Version
:experimental:
:icons: font
//:icon-set: fa
////
You can use font-based icons from any of the following icon sets in your PDF document:
fa - Font Awesome 4 (default, deprecated)
fas - Font Awesome - Solid
fab - Font Awesome - Brands
far - Font Awesome - Regular
fi - Foundation Icons
pf - Payment font (deprecated)
////
// saut da page <<<


== INFRA

[plantuml, target=vmware-01, format=png]
----
nwdiag {
      
  network STORAGE-vSwitch2 {
      address = "192.168.1.0/24"

      FreeNAS [address = "        .254"];
      ESX01 [address = "      .1"];
      ESX02 [address = "      .2"];
  }

  network MGMT-vSwitch0 {
      address = "172.16.20.0/24";

      ESX01 [address = "      .1"];
      ESX02 [address = "      .2"];
      FreeNAS [address = "      .4"];
      DC01 [address = "      .10"];
      ESX03 [address = "      .3"];
  }

}
----

== Documentation

http://www.vmware.com/info?id=1414

== Installation des ESXi

=== ESX-A

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs8-001.png[] 

=== ESX-B

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs8-001bis.png[] 

=== ESX-C

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs8-001ter.png[] 

== Installation FreeNAS V9.10

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs8-001-quart.png[] 

2 cartes nic : 

- une pour le réseau de Gestion (MGMT)
- l'autre pour la cible iSCSI

_boot via bios_

== Installation win2k22

=== ADDS / DNS

*valdamp.local*

=== Enregistrement DNS pour les 3 ESXi

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs8-01.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs8-02.png[] 

== Configuration FREENAS

=== Ajout d'un portail

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-03.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-04.png[] 

=== Gestion des initiateurs

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-05.png[]

[WARNING]
Mettre _ALL_ ci-dessous, en lieu et place des deux IP 192.168.1.1 et 192.168.1.2

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-06.png[]

=== Ajout d'UNE cible iSCSI

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-07.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-08.png[] 

=== Faire le lien entre les partages iSCSI et les disques durs

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-09.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-11.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-12.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-13.png[]

=== Ajout cible / extensions

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-14.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-15.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-16.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-17.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-17bis.png[] 

=== Activation du service iSCSI

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-18.png[] 

les LUNs iSCSI sont prêts à être montés et doivent être diffusés sur le réseau iSCSI.

== Enregistrement FreeNAS dans le DNS

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-19.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-20.png[]

== Création du datastore local sur ESX-C

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore01.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore02.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore03.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore04.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore05.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore06.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/datastore07.png[] 

== Déploiement du vcsa

=== Etape 1 : déploiement de la VM _vcsa_

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-21.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-22.png[] 

https://kb.vmware.com/s/article/60229

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-23.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-24.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-25.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-26.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-27.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-28.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-29.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-30.png[] 

20 minutes plus tard...

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-31.png[]

=== Créer un enregistrement dans le dns

image:ROOT:AIS-VMWARE-V8-01-INFRA/dnsvcsa01.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/dnsvcsa02.png[] 

=== Etape 2 : Configuration du vcsa

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-32.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-33.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-34.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-35.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-36.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-37.png[] 

20 minutes plus tard...

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-38.png[] 

== Connexion au vcsa

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-39.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-40.png[] 

== Configuration du vcsa

=== Création d'un centre de données

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-41.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-42.png[]

=== Ajout des hôtes ESX-A, ES-B et ESX-C

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-43.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-44.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-45.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-46.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-47.png[] 

https://kb.vmware.com/s/article/89519

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-48.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-49.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-50.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-51.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-52.png[]

== Gestion du réseau

=== ESX-A

==== Création de vSwitch1 (Réseau VMs)

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-53.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-54.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-55.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-56.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-57.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-58.png[] 

==== Création de vSwitch2 pour le stockage

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-59.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-60.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-61.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-62.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-63.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-64.png[] 

=== ESX-B

Répétez les mêmes étapes pour la création de vSwitch1 et vSwitch2 sur le même modèle.

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-65.png[]

=== Configuration des initiateurs iSCSI

==== ESX-A

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-65.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-66.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-67.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-68.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-69.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-70.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-71.png[] 

==== ESX-B

idem

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-72.png[]

== Connexion des initiateurs iSCSI à leur cible

=== ESX-A

==== Ajout d'un adaptateur logiciel iSCSI

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-73.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-74.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-75.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-76.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-77.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-78.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-79.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-80.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-81.png[]

=== ESX-B

idem

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-82.png[]

== Création des banques de données

=== ESX-A

==== LUN-1

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-83.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-84.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-85.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-86.png[]



image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-87.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-88.png[]

==== LUN-2

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-89.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-90.png[]



image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-91.png[]

==== LUN-3

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-92.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-93.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-94.png[]

==== Synthèse ESX-A

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-95.png[]

=== ESX-B

Faire de même

==== Synthèse ESX-B

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-96.png[]

== Chargement d'ISOs dans la LUN-1

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-97.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-98.png[]


[NOTE]
Voir problème de certificat... +
https://kb.vmware.com/s/article/2147256 +
Attention Mozilla possède son propre magasin de certificats

[WARNING]
J'ai rencontré des soucis pour charger les ISOs directement sur LUN-1 bien qu'ayant suivi la procédure d'intégration des certificats. +
J'ai pu contourner en chargeant les ISOs dans le datastore local de ESX-C puis déplacement de ces mêmes ISOs vers le LUN-1 dans un second temps... (à voir). +
Notez également que le certificat est valable sur vcsa.valdamp.local et pas sur l'adresse IP. Nous avions vu ça quand nous avions traité le sujet des autorités racine de certification. Nous vions vu que le _common name_ du certificat devait correspondre au nom DNS du site ; c'est pareil ici. Visualisez bien les informations présentes dans le certificat.


== Sur ESX-A et LUN-1

Installer la VM win2k19-test-vmotion
Installer la VM win2k19-test-ft

== Sur ESX-B et LUN-1

Installer la vm ubuntu-test-ha

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-99.png[]

== Création d'un réseau de production sur vSwitch1

=== Sur ESX-A

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-100.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-101.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-102.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-103.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-104.png[]

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-105.png[]

=== Sur ESX-B

Répéter les mêmes étapes.

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-106.png[]

== Migrer ces VMs sur ce réseau

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-107.png[] 

image:ROOT:AIS-VMWARE-V8-01-INFRA/vs08-108.png[]

== Sur ESX-A et LUN-1 / réseau PRODUCTION

Installer la VM win2k19-test-ft











